panic() {
	echo "$*" >&2
	exit 1
}
echo `dirname $0` || panic "couldn't chdir"

for arch in x86_64
do
	(
		echo Building packages in $arch..
		cd $arch
		repo-add -k EED70F01F0600C94539E433E3A83C912C5B1CBE5 -s -v echno.db.tar.gz *.tar.zst
		find ./ -type l -exec sh -c 'cp --remove-destination $(readlink "{}") "{}"' \;
		rm -f echno.db.tar.gz* echno.files.tar.gz*
		cd ..
	)
done
