Add to pacman.conf
```
[echno]
SigLevel = Required TrustedOnly
Server = https://gitlab.com/echnobas/$repo/-/raw/main/$arch
```
```sh
sudo pacman-key --add pacman.pub
sudo pacman-key --lsign EED70F01F0600C94539E433E3A83C912C5B1CBE5
```


PKGBUILDs are available for most packages

PKGBUILDS are not included for
- wine-tkg builds
